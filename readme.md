Testes Integrados de API
===

Este projeto tem como objetivo testar de maneira integrada as APIs criadas.

### Tecnologias

* node.js
* mocha.js
* chai.js
* chai-http.js
* grunt

### Estrutura

O projeto está dividido em api, config e services.

* **api** estão todos os testes;
* **config** estão as configurações de url e a parte de suporte para a chamada das apis;
* **services** são todas as chamadas para cada método das apis;


### Config

A configuração do projeto se dá através do arquivo *config.<env>.js* localizado no diretório raiz. 
    
A propriedade <env> do nome desse arquivo determina qual arquivo config específico, por ambiente, contendo a url e demais dados a serem usados nos testes. 
    
Outro ponto importante é o arquivo *util.js* que contém todas as chamadas restfull básicas, como *post*, *get*, *put*, etc.


### Services

Essa parte de services é onde tem a configuração de todas as chamadas para as APIs que serão testadas. Para cada API é criado um arquivo *js* com a nomenclatura **nomeDaApiService.js**.

Na estrutura services existe um arquivo *urlService.js* qual busca a url do ambiente que está sendo testado e devolve através do método *getFullUrlPrincipalApi* a url base da api a ser chamada. Para os métodos da api basta passar o parâmetro complementando a url. Ex.: `getFullUrlPrincipalApi('/token')`.


### Api

Esse diretório é específico para armazenar os testes integrados. Os arquivos devem conter a seguinte nomenclatura **funcionalidadeATestar.js**.


### Rodando o Projeto

Na linha de comando e no diretório raiz do projeto execute o comando:

```shell
npm install
```

O comando acima restaurará os pacotes necessários para que consiga rodar os testes.

```shell
npm run <env>.test
```

Ex.: *npm run prod.test*


### Acessando o Reporte de Resultados de Teste

O arquivo <diretorio_raiz_do_projeto>/mochawesome-report/mochawesome.html contém os resultados dos testes após cada execução.
