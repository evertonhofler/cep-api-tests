'use strict';
var chai = require('chai');
var config = require('../config.' + process.env.NODE_ENV);
var {describe, before, it } = require('mocha');
var expect = chai.expect;
var assert = chai.assert;

var cepService = require('../services/cepService');

var cepService;

var validCep;
var validCepLogradouro;
var validCepBairro;
var validCepCidade;
var validCepEstado;

before("", () => {
    cepService = new cepService(this);

    validCep = "01307001";
    validCepLogradouro = config.util.VALID_CEP_01307001_LOGRADOURO;
    validCepBairro = config.util.VALID_CEP_01307001_BAIRRO;
    validCepCidade = config.util.VALID_CEP_01307001_CIDADE;
    validCepEstado = config.util.VALID_CEP_01307001_ESTADO;
});

describe('Testes na API de CEP', () => {

    it('Obtém um CEP válido e valida os dados retornados', async () => {
        let responseGetCep = await cepService.getCep(validCep);
        expect(config.util.HTTP.OK, 'Deve retornar o HTTP status code').to.equal(responseGetCep.statusCode);
        expect(validCep, 'Deve retornar o cep').to.equal(responseGetCep.body.cep);
        expect(validCepLogradouro, 'Deve retornar o logradouro').to.equal(responseGetCep.body.logradouro);
        expect(validCepBairro, 'Deve retornar o bairro').to.equal(responseGetCep.body.bairro);
        expect(validCepCidade, 'Deve retornar o cidade').to.equal(responseGetCep.body.cidade);
        expect(validCepEstado, 'Deve retornar o estado').to.equal(responseGetCep.body.estado);
    });

});