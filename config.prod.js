config = { 
    "urlCepApi": "http://api.postmon.com.br/v1",
    "OAuth": {"password":"112233445566","user":"qa", "grant_type":"password"},
    "finduser":"9e3e4779-f2ce-417b-b686-f499094a2033",
    "util": {
        "HTTP": {
            "OK": 200,
            "CREATED": 201,
            "ACCEPTED": 202,
            "NO_CONTENT": 204,
            "BAD_REQUEST": 400,
            "UNAUTHORIZED": 401,
            "FORBIDDEN": 403,
            "NOT_FOUND": 404,
            "METHOD_NOT_ALLOWED": 405,
            "UNPROCESSABLE_ENTITY": 422,
            "INTERNAL_SERVER_ERROR": 500                        
        },
        "HIGH_SECONDS": 500000,
        "DEFAULT_SECONDS": 100000,
        "CONTENT_TYPE": "Content-Type",
        "AUTHORIZATION": "Authorization",
        "OCP_APIM_SUBSCRIPTION_KEY": "Ocp-Apim-Subscription-Key",
        "STATUSCODE": "Ok",
        "STATUSCODE_BAD_REQUEST": "BadRequest",
        "SUCCESS": true,
        "NOT_SUCCESS": false,
        "ACCEPT":"Accept",
        "BEARER": "Bearer ",
        "BASIC": "Basic ",
        "ContentType": {
            "JSON": "application/json",
            "FORM_URL_ENCODED": "application/x-www-form-urlencoded",
            "OCP_APIM_SUBSCRIPTION_KEY": "a8d5b1db64234b4db407be99a398a00b"
        },
        "DEFAULT_HEADER": { 
            "headers": { 
                "Authorization": "",
                "Ocp-Apim-Subscription-Key": ""
            }
        },

        "VALID_CEP_01307001_LOGRADOURO": "Rua Frei Caneca",
        "VALID_CEP_01307001_BAIRRO": "Consolação",
        "VALID_CEP_01307001_CIDADE": "São Paulo",
        "VALID_CEP_01307001_ESTADO": "SP",
    }

};

module.exports = config