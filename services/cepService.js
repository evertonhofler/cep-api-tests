'use strict';
var Util = require('../util')
var UrlService = require('../services/urlService')

var CepService = function(that) {
    this.util = new Util(that);
    this.urlService = new UrlService(that);
    this.baseUrl = this.urlService.getFullUrlPrincipalApi(); 
};

CepService.prototype.getCep = function(cep)
{
     return this.util.getUrl(this.baseUrl, this.util.getHeaderJson(), '/cep/' + cep);
}

module.exports = CepService