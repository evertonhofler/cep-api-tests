'use strict';
var chai = require('chai'), chaiHttp = require('chai-http');

var basic;
var env;

var Util = function(that) {
    chai.use(chaiHttp);
    this.that = that;    
};

Util.prototype.getEnv = function() {
    return env;
};

Util.prototype.getHeaderFormUrlEncoded = function(token) {
    var header = this.getDefaultHeader(token);
    header[config.util.CONTENT_TYPE] = config.util.ContentType.FORM_URL_ENCODED;
    return header;
};

Util.prototype.getUrls = function() {
    return config.urls.environment;
};

Util.prototype.getHeaderJson = function(authorization) {
    var header = this.getDefaultHeader(authorization);
    header[config.util.OCP_APIM_SUBSCRIPTION_KEY] = config.util.ContentType.OCP_APIM_SUBSCRIPTION_KEY;
    return header;
};

Util.prototype.getOAuthHeader = function() {
    var header = {};
    header[config.util.CONTENT_TYPE] = config.util.ContentType.FORM_URL_ENCODED;
    header[config.util.OCP_APIM_SUBSCRIPTION_KEY] = config.util.ContentType.OCP_APIM_SUBSCRIPTION_KEY;
    return header;
};

Util.prototype.getInvalidOAuthHeader = function() {
    var header = {};    
    header[config.util.ACCEPT] = config.util.ContentType.ACCEPT;
    header[config.util.CONTENT_TYPE] = config.util.ContentType.FORM_URL_ENCODED;
    return header;
};

Util.prototype.getInvalidTokenHeader = function(token) {
    var header = config.util.INVALID_HEADER2;    
    header.client_id = config.env[this.getEnv()].CloudLoyaltyClientId;
    return header;
};

Util.prototype.getInvalidclientIdtHeader = function(token) {
    var header = config.util.INVALID_HEADER;
    header.access_token = token;    
    return header;
};

Util.prototype.getInvalidTokenAndclientIdtHeader = function(token) {
    var header = config.util.INVALID_HEADER3;    
    return header;
};

Util.prototype.getDefaultHeader = function(authorization) {
    var header = config.util.DEFAULT_HEADER;
    header.Authorization = "Bearer " + authorization;
    return header;
};

Util.prototype.getUrl = function(baseUrl, header, url) {
    return chai.request(baseUrl)
        .get(url)
        .set(header);
};

Util.prototype.postUrl = function(baseUrl, body, header, url) {
    return chai.request(baseUrl).post(url)
        .set(header)        
        .send(body);
};

Util.prototype.deleteUrl = function(baseUrl, body, header, url) {
    return chai.request(baseUrl)
        .del(url)
        .set(header)
        .send(body);
};

Util.prototype.putUrl = function(baseUrl, body, header, url) {
    return chai.request(baseUrl)
        .put(url)
        .set(header)
        .send(body);
};

Util.prototype.patchUrl = function(baseUrl, body, header, url) {
        return chai.request(baseUrl)
            .patch(url)
            .set(header)
            .send(body);
    };
    
Util.prototype.timeout = function(miliseconds) {
    this.that.timeout(miliseconds);
};

Util.prototype.getParameterByName = function(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

Util.prototype.gera_random = function(n) {
    var ranNum = Math.round(Math.random() * n);
    return ranNum;
}

Util.prototype.addDays = function(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
}

Util.prototype.mod = function(dividendo, divisor) {
    return Math.round(dividendo - (Math.floor(dividendo / divisor) * divisor));
}

module.exports = Util