'use strict';
var Util = require('../util')
var UrlService = require('../services/urlService')
var config = require('../config.' + process.env.NODE_ENV)

var UrlService = function(that) {
    this.util = new Util(that);
    this.url = config.urlCepApi;
};

UrlService.prototype.getFullUrlPrincipalApi = function() {
    return this.url;
};

UrlService.prototype.getFullUrlLiveApi = function() {
    return this.url;
};

module.exports = UrlService